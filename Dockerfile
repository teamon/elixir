FROM elixir:1.7.4-alpine
LABEL version="1.7.4-otp-21"

# install build dependencies
RUN apk add --no-cache --update bash git

# prepare build dir
RUN mkdir /app
WORKDIR /app

# install hex + rebar
RUN mix local.hex --force && \
    mix local.rebar --force

# set build ENV
ENV MIX_ENV=prod
