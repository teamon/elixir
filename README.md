# Elixir builder image

Alpine-based elixir builder

### Available versions
- `elixir:1.7.4-otp-21`

### Updating
Edit `Dockerfile` and update `version` label. 
New release will be built after pushing to master.